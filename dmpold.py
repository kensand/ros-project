import numpy as np
import math
def phase(time, tottime, alpha=(-1. * math.log(0.01))):
    t = math.exp(-1. * alpha * time / tottime)
    return t

#traj is 2dim nparray, axis0 is time, axis1 is joints
def learnDMP(K, D, traj, times):
    if len(times) != len(traj):
        print("Times do not match traj length", len(times), len(traj))
    print(traj.shape[1])
    dtime = np.swapaxes(np.array([times]* traj.shape[1]), 0, 1)
    speed = np.concatenate(((np.diff(traj, n=1, axis=0) / np.diff(dtime, n=1, axis=0)), np.zeros((1, traj.shape[1]))), axis=0)
    #accel = np.concatenate((np.zeros((1, traj.shape[1])), (np.diff(traj, n=2, axis=0) / np.diff(dtime, n=1, axis=0)[:traj.shape[0] - 2]), np.zeros((1, traj.shape[1]))), axis=0)
    accel = np.concatenate((np.diff(traj, n=2, axis=0), np.zeros((2, traj.shape[1]))), axis=0)
    #accel = np.concatenate((np.zeros((1, traj.shape[1])), np.diff(speed, n=1, axis=0) ), axis=0)
    g = traj[-1]
    print("learned goal:", g)
    print("times: ", times)
    xstart = traj[0]
    ftarg = []
    prevtau = 1.
    T = (times[-1] - times[0])
    taus = [phase(time, T) for time in times]
    for i, step in enumerate(traj):
	a = (( T * accel[i] - D * speed[i]) / K)
	b =  (g - traj[i]) 
	c =  (g - traj[0]) * taus[i]
	print("abc = ", a, b, c)
	print("T = ", T)
	print("position = ", traj[i])
	print("accel = ", accel[i])
	print("speed = ", speed[i])
	print("tau = ", taus[i])
	print("ftarg = ", a - b + c)
        ftarg.append(a - b + c)
	prevtau = taus[i]
        
        
    return np.array(ftarg), np.array(taus)

def getF(f, taus, tau):
    #print("f=",f, "taus=",taus, "tau=",tau)
    if tau in taus:
        return f[np.where(taus == tau)][0]
    for i in range(len(taus) - 1):
        if (tau < taus[i] and tau > taus[i+1]) or (tau > taus[i] and tau < taus[i+1]):
            slope = (f[i+1] - f[i]) / (taus[i+1] - taus[i])
            res = f[i] + slope * (tau - taus[i])
            return res
    return None


#dmpparameters is 2dim nparray, axis0 is time, axis1 is joints
def useDMP(DMPparameters, prevtaus, K, D, startpos, startvel, goalpos, tlen, dt):
    tsteps = int(math.ceil(tlen / dt))
    times = [i * dt for i in range(tsteps)]
    taus = np.array([phase(i * dt, tlen) for i in range(tsteps)])
    curv = startvel
    curp = startpos
    cura = np.zeros(startpos.shape)
    traj = [curp]
    vels = [curv]
    accels = [cura]
    prevtau = 1
    for i, tau in enumerate(taus):
        #print(curp, curv, cura)
        #print(tau)
        f = getF(DMPparameters, prevtaus, tau)
        print("f = ", f)

        a = K * (goalpos - curp)
        b = D * curv
        c = K * (goalpos - startpos) * tau
        d = K * f
        print('abcd:',a, b, c, d)
        cura = (a - b - c + d) / tlen
        curv = (curv + cura * dt) / tlen
        curp = curp + curv * dt
        traj.append(curp)
        vels.append(curv)
        accels.append(cura)
        prevtau = tau

    return np.array(traj), np.array(vels), np.array(accels)


def main():   
    N = 100
    steps = [float(x) / 10 for x in range(N)]
    path = [[1.0, 1.0], [2., 2.], [3., 4.], [6., 8.]]
    K = 5.
    D = 2. * math.sqrt(K)
    traj = np.array(path)
    print("Original Trajectory: \n",traj)
    dmp, taus = learnDMP(K, D, traj, range(4))
    print("DMPparameters: \n",dmp)
    restraj, vels, accels = useDMP(dmp, taus, K, D, np.zeros(2), np.zeros(2), np.array([8., 7.]), 20., 1.)
    print("Result Trajectory: \n", restraj)
    print("Result Velocities: \n", vels)
    print("Result Accelerations: \n", accels)




if __name__ == "__main__":
    main()




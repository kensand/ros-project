#!/bin/python
import csv
import argparse

ap = argparse.ArgumentParser()
ap.add_argument('-f', '--file', default='output.csv')
ap.add_argument('-l', '--limit', default=10, type=int)
ap.add_argument('-i', '--interval', default=100000000, type=int)
args = ap.parse_args()
lasttuple = None
lasttime = 0
count = 0
with open(args.file, 'r+') as f:
    r = csv.reader(f)
    header = next(r)
    for line in r:
        if count >= args.limit:
            break
        try:
            if lasttime + args.interval < int(line[0]):
                if lasttuple and not len([(x,y) for x,y in zip(lasttuple[12:20], line[12:20]) if abs(float(x)-float(y)) > 0.1]):
                    pass
                else:
                    print("------------------------------------")
                    print(header[0], ":", line[0])
                    for i in range(8):
                        print(header[i+12], ":", line[i+12])
                    count+=1
                    lasttime = int(line[0])
                    lasttuple = line
        except Exception as e:
            print("Error reading line: ", e)

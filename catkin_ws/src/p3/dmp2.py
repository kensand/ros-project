#!/usr/bin/env python
import math
import numpy as np
import rospy
import rosbag
from geometry_msgs.msg import PoseStamped
from intera_core_msgs.msg import EndpointState
from intera_core_msgs.srv import (SolvePositionIK, SolvePositionIKRequest)
from visualization_msgs.msg import (Marker, MarkerArray)
from std_msgs.msg import String
import intera_interface
import intera_external_devices
import argparse
import os 
from intera_interface import CHECK_VERSION
jointangletolerance = 0.01
def main():
	rospy.init_node('ik_replay')
	bag = rosbag.Bag("arm.bag", 'r')
	points = [] 
	for topic,msg, t in bag.read_messages(topics=['/robot/limb/right/endpoint_state']):
		points.append(msg)
	bag.close()

	filtered = [points[0]]
	delay = 0.01
	time_list = []
	endpoint_list = []

	for p in points:
		time = float(p.header.stamp.secs) + float(p.header.stamp.nsecs)/1000000000.
		ltime = float(filtered[-1].header.stamp.secs) + float(filtered[-1].header.stamp.nsecs)/1000000000.
		n = p.pose
		new = [n.position.x, n.position.y, n.position.z, n.orientation.x, n.orientation.y, n.orientation.z, n.orientation.w]
		n = filtered[-1].pose
		old = [n.position.x, n.position.y, n.position.z, n.orientation.x, n.orientation.y, n.orientation.z, n.orientation.w]	
		#skip if there is no physical position changes or time changes
		if len([x for x,y in zip(new, old) if abs(x-y) > jointangletolerance]) > 0 and time > ltime + delay:
			filtered.append(p)
			endpoint_list.append(new)
			time_list.append(time)
	k = 1.
	traj = np.array(endpoint_list)
	times = np.array(time_list)
	f_target, phase = learn(traj, times, k) 
	print(f_target, phase)
	restraj = reproduce(phase, f_target, traj[0], traj[-1], 1., k, 300.)
	print(restraj)
	
	endpoint_list = []	
	for p in restraj:
		s = PoseStamped()
		s.pose.position.x = p[0]
		s.pose.position.y = p[1]
		s.pose.position.z = p[2]
		q = p[3:7]				  #quaternion
		total = math.sqrt(sum([x * x for x in q]))
		if total != 0.:
			q /= total
		else:
			q = [0.,0.,0.,1.]

		s.pose.orientation.x = q[0]
		s.pose.orientation.y = q[1]
		s.pose.orientation.z = q[2]
		s.pose.orientation.w = q[3]
		endpoint_list.append(s)	

	filtered = endpoint_list
	count = 0
	joint = []
	start = ik(filtered[0])
	while not start and count < len(filtered):
		start = ik(filtered[count])
		count += 1
	joint += start.joints #return joint state 
	for f in filtered[1:]:
		res = ik(f,joint[-1])
		if res:
			joint += res.joints

	limb = intera_interface.Limb('right')
	limb.move_to_neutral(timeout=1.)
	count = 0
	for j in joint:
		limb.move_to_joint_positions(dict(zip(j.name, j.position)), timeout = delay)
		t = rospy.get_time()
		while not limbdone(limb, dict(zip(j.name, j.position))) and t + delay < rospy.get_time():
			pass 
		print(count, len(joint))
		count += 1
	#raw_input("Press enter to reset")
	print("Input goal", traj[-1])
	print("Result goal", restraj[-1])
	limb.move_to_neutral(timeout=1.)
def ik(position, seed = None):    #position = endpoint state position
	ns = "ExternalTools/right/PositionKinematicsNode/IKService"
	service = rospy.ServiceProxy(ns, SolvePositionIK)
	request = SolvePositionIKRequest()
	request.pose_stamp.append(position)
	request.tip_names.append('right_hand')
	if seed:
		request.seed_angles.append(seed)
		request.seed_mode = request.SEED_USER
	try:
		rospy.wait_for_service(ns, 30)
		respond = service(request)
		if respond.result_type[0] > 0:
			return respond
	except Exception, e:
		rospy.logerr(e)
	return False	
def limbdone(limb, pos):
	for n,a in limb.joint_angles().items():
		if abs(a - pos[n]) > jointangletolerance:
			return False
	return True

def calPhase(t, tau):
	alpha = -1.*math.log(0.01)
	return math.exp((-alpha/tau)*t)	

#def learn(traj, times, k):
#	times -= times[0]   #set the times start from 0
#	tau = times[-1]     # set tau equal to total time interval
#	dt = np.swapaxes(np.repeat([np.diff(times, n = 1,axis = 0)], traj.shape[1], 0), 0, 1 ) 
#	v = np.diff(traj, n = 1, axis = 0) / dt	
#	a = np.diff(v, n = 1, axis = 0) / dt[:-1, :]
#	v = np.concatenate((v,np.zeros((1,traj.shape[1]))), axis=0)
#	a = np.concatenate((a,np.zeros((2,traj.shape[1]))), axis=0)
#	d = 2*math.sqrt(k)
#       phase = np.array([calPhase(t,tau) for t in times])
#	goal = np.repeat([traj[-1]] , traj.shape[0], axis = 0)
#	f1 = ((tau*a)+(d*v))/k 
#	f2 = goal-traj
#	f3 = np.repeat([traj[-1]-traj[0]],traj.shape[0],0)* np.swapaxes(np.repeat([phase], 7, axis=0), 0, 1)
#	f_target = ((tau*a)+(d*v))/k - (goal-traj) + np.repeat([traj[-1]-traj[0]],traj.shape[0],0)* np.swapaxes(np.repeat([phase], 7), 0, 1)
#	f_target = f1+f2+f3
#	return f_target, phase
 
def learn(traj, times, k):
	times -= times[0]   #set the times start from 0
	tau = times[-1]     # set tau equal to total time interval
	dt = np.swapaxes(np.repeat([np.diff(times, n = 1,axis = 0)], traj.shape[1], 0), 0, 1 ) 
	v = np.diff(traj, n = 1, axis = 0) / dt	
	a = (np.diff(traj, n = 2, axis = 0) / dt[:-1, :])# / tau
	v = np.concatenate((v,np.zeros((1,traj.shape[1]))), axis=0)
	a = np.concatenate((a,np.zeros((2,traj.shape[1]))), axis=0)
	d = 2*math.sqrt(k)
        phase = np.array([calPhase(t,tau) for t in times])
	goal = np.repeat([traj[-1]] , traj.shape[0], axis = 0)
	f1 = ((tau*a)+(d*v))/k 
	f2 = goal-traj
	f3 = np.repeat([traj[-1]-traj[0]],traj.shape[0],0)* np.swapaxes(np.repeat([phase], 7, axis=0), 0, 1)
#	f_target = ((tau*a)+(d*v))/k - (goal-traj) + np.repeat([traj[-1]-traj[0]],traj.shape[0],0)* np.swapaxes(np.repeat([phase], 7), 0, 1)
	f_target = (f1-f2+f3)#/  np.swapaxes(np.repeat([phase], 7, axis=0), 0, 1)

	return f_target, phase
 
def reproduce(phase, f_target, start, goal, dt, k, tau):
	t_step = math.floor(tau/dt)
	phase_new = [calPhase(t*dt, tau) for t in range (int(t_step))]
	f_target_new = [linear_interpolation(phase, f_target, s) for s in phase_new]  
	x = start  
	v = np.zeros(f_target.shape[1])
	a = np.zeros(f_target.shape[1])
	result = [x]                  #new trajectory
	d = 2* math.sqrt(k)
	for i, f in enumerate(f_target_new):
		g1 = k*(goal-x)
		g2 = d*v
		g3 = k*(goal-start)*phase_new[i]
		g4 = k*f
		xdot = v / tau
		a = (g1-g2-g3+g4) / tau
		v = v + a * dt
		x = x + xdot * dt
		result.append(x)
	return np.array(result)

def linear_interpolation(x,y,x_new):
	if x_new in x:
		return y[x.tolist().index(x_new)]
	for v in range (len(x) - 1):
		 if x_new < x[v] and x[v+1] < x_new:
			return y[v] + (x[v] - x_new) * (y[v+1]-y[v]) / (x[v+1] - x[v])


	
if __name__ == '__main__':
    main()	
	
	

import numpy as np
import math

def phase(time, tottime, alpha=(-1. * math.log(0.01))):
	t = math.exp(-1. * alpha * time / tottime)
	return t
def lininter(x,y,xn):
	if xn in x:
		return y[x.tolist().index(xnew)]

def linear_interpolation(x,y,x_new):
	if x_new in x:
		return y[x.tolist().index(x_new)]
	for v in range (len(x) - 1):
		if x_new < x[v] and x[v+1] < x_new:
			return y[v] + (x_new - x[v]) * (y[v+1]-y[v]) / (x[v+1] - x[v])


def learn(traj, times, k):
	times -= times[0]   #set the times start from 0
	tau = times[-1]	 # set tau equal to total time interval
	dt = np.swapaxes(np.repeat([np.diff(times, n = 1,axis = 0)], traj.shape[1], 0), 0, 1 ) 

	v = np.diff(traj, n = 1, axis = 0) / dt	
	#a = (np.diff(traj, n = 2, axis = 0) / dt[:-1, :])# / tau
	a = np.diff(traj, n=2, axis = 0) / dt[:-1,:]
	v = np.concatenate((np.zeros((1,traj.shape[1])), v), axis=0)
	a = np.concatenate((np.zeros((2,traj.shape[1])), a), axis=0)
	print("a=", a)
	print("v=", v)
	d = 2*math.sqrt(k)
	phase = np.array([calPhase(t,tau) for t in times])
	goal = np.repeat([traj[-1]] , traj.shape[0], axis = 0)
	#f1 = ((tau*a)+(d*v))/k 
	#f2 = goal-traj
	#f3 = np.repeat([traj[-1]-traj[0]],traj.shape[0],0)* np.swapaxes(np.repeat([phase], traj.shape[1], axis=0), 0, 1)
#	f_target = ((tau*a)+(d*v))/k - (goal-traj) + np.repeat([traj[-1]-traj[0]],traj.shape[0],0)* np.swapaxes(np.repeat([phase], 7), 0, 1)
	#f_target = (f1-f2+f3)#/  np.swapaxes(np.repeat([phase], 7, axis=0), 0, 1)
	print(phase.shape)
	phasearr = np.swapaxes(np.repeat([phase], traj.shape[1], axis=0), 0, 1)
	f4 = np.repeat([traj[-1] - traj[0]], traj.shape[0], axis=0) * phasearr
	f1 = a * tau
	f2 = (goal - traj)
	f3 = d * v# * phasearr
	#print("f3=",f3)
	f_target = (f1 + f3) / k - f2 + f4
	f_target = f_target
	return f_target, phase
 
def reproduce(phase, f_target, start, goal, dt, k, tau):
	t_step = math.floor(tau/dt)
	phase_new = [calPhase(t*dt, tau) for t in range (int(t_step))]
	f_target_new = [linear_interpolation(phase, f_target, s) for s in phase_new]  
	#print("ftarg=",f_target_new)
	x = start
	v = np.zeros(f_target.shape[1])
	a = np.zeros(f_target.shape[1])
	result = []
	d = 2* math.sqrt(k)
	for i, f in enumerate(f_target_new):
		g1 = k*(goal-x)
		g2 = d*v
		g3 = k*(goal-start)*phase_new[i]
		g4 = k*f
		#print("g2", g2)
		#print(g1, g2, g3, g4)
		xdot = v / tau
		a = (g1-g2-g3+g4) / tau
		v = v + a * dt
		x = x + v * dt
		result.append(x)
	return np.array(result)

def calPhase(t, tau):
	alpha = -1.*math.log(0.01)
	return math.exp((-alpha/tau)*t)	


def main():   
	N = 100
	steps = [float(x) / 10 for x in range(N)]
	path = [[1.0, 1.0], [2., 2.], [3., 4.], [6., 8.]]
	K = 10.
	D = 2. * math.sqrt(K)
	traj = np.array(path)
	print("Original Trajectory: \n",traj)
	dmp, taus = learn(traj, np.array(range(4)), K)
	print("DMPparameters: \n",dmp)
	restraj = reproduce(taus, dmp, np.array([1., 1.]), np.array([6., 8.]), 1., K, 8.)
	print("Result Trajectory: \n", restraj)
	restraj = reproduce(taus, dmp, np.array([1., 1.]), np.array([8., 7.]), 1., K, 8.)
	print("Result Trajectory: \n", restraj)

import random
def main2():   
	N = 100.
	scale = 0.01
	steps = [1.+scale * float(x) /float(N) for x in range(int(N))]
	path = [[i, i*i-5*i + 8] for i in steps]
	K = 0.01
	traj = np.array(path)
	print("Original Trajectory: \n",traj)
	dmp, taus = learn(traj, np.array(range(int(N))), K)
	print("DMPparameters: \n",dmp)
	restraj = reproduce(taus, dmp, np.array([1., 1.]), np.array([scale, scale * scale]), 1., K, N)
	print("Result Trajectory: \n", restraj)
	restraj = reproduce(taus, dmp, np.array([1., 1.]), np.array([0.9 * scale, 1.05 * scale]), 0.01, K, N)
	print("Result Trajectory: \n", restraj)









if __name__ == "__main__":
	main2()




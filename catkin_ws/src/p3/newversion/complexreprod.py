#!/usr/bin/env python
import rospy
import rosbag
import csv
from intera_core_msgs.msg import (EndpointState, IOComponentCommand)
from geometry_msgs.msg import PoseStamped
from intera_core_msgs.srv import (SolvePositionIK, SolvePositionIKRequest)
from sensor_msgs.msg import JointState
from visualization_msgs.msg import (Marker, MarkerArray)
from std_msgs.msg import String
import intera_interface
import intera_external_devices
import pickle
import argparse
import subprocess
import signal
import os
from intera_interface import CHECK_VERSION
import dmp
import numpy as np
import math
import json
import matplotlib.pyplot as plt
from util import *
#from rviz_visual_tools import RvizVisualTools

jointangletolerance = 0.05
joint_tolerance = 0.01
#calback function for recording a subscriber

def main(graphs=True):
	rospy.init_node('reproduce')
	#graphs=False
	approachpoints, approachtimes = loadbag('approach4.bag')
	movepoints, movetimes = loadbag('move2.bag')

	#known good parameters, to save time instead of executing policy search every time. Set to None to execute policy search
	approachparams = [1.1, 1.0, 0.8, 0.8]
	moveparams = [1.2, 1.2, 0.8, 0.8]


	reset()
	print(approachpoints.shape)

	#remove points that cause problems
	approachpoints = approachpoints[:-5]
	approachtimes = approachtimes[:-5]
	movepoints = movepoints[5:]
	movetimes = movetimes[5:]
	approachseed=None
	approachphase = np.array([dmp.calcphase(x, approachtimes[-1]) for x in approachtimes])	
	movephase = np.array([dmp.calcphase(x, movetimes[-1]) for x in movetimes])	


	allphase = np.concatenate((-1 + -1 * approachphase, -1 * movephase), axis=0)
	
	
	#the transformation of the objects location from it's original location (.75, 0, 1.)
	objtrans = np.array([0.0, 0.0, 0., 0., 0., 0., 0.])
	#call the reproduce function below main to reproduce trajectories using the dmp algo
	approachtraj, approachends, approacharr, approachphase = reproduce(approachpoints, approachtimes, startseed=approachseed, endtransform=objtrans + np.array([0., -0.06, 0., 0., 0., 0., 0.]), parameters=approachparams, K=700)

	movetraj, moveends, movearr, movephase = reproduce(movepoints, movetimes, starttransform=objtrans + np.array([0., 0., 0., 0., 0., 0., 0.]), endtransform=np.array([0., 0., 0., 0., 0., 0., 0.]), startseed=approachtraj[-1], parameters=moveparams, K=1000)

	#show the path
	showPath(approachends + moveends)
	newallphase = np.concatenate((-1. - approachphase, -1. * movephase), axis=0)
	actual = np.concatenate((approachpoints, movepoints), axis=0)
	newtrajs = np.concatenate((approacharr, movearr), axis=0)

	#plot trajectory graphs
	if graphs:
		labels=['x-position', 'y-position', 'z-position', 'x-orientation', 'y-orientation', 'z-orientation', 'w-orientation']
		
		for i in range(actual.shape[1]):
			plotlocs =[1, 2, 3, 5, 6, 7, 8] 
			plt.subplot(2, 4, plotlocs[i])
			a1 = plt.plot(allphase, actual[:,i], 'r.-', label='Demonstration')
			a2 = plt.plot(newallphase, newtrajs[:,i], 'b.-', label='Reproduction')
	
			plt.xlabel("phase")
			plt.ylabel(labels[i])
			if plotlocs[i] == 3:
				#plt.subplot(2,4,4)
                                plt.legend(loc='best ', bbox_to_anchor=(1.5, 0.5))

		plt.show()
	
	#wait for enter then execute the trajectory
	raw_input("Press enter to execute movement\n")
	l = intera_interface.Gripper('right_gripper')
	l.open()
	replay(approachtraj)
	print("approach done")

	#grip the object here
	l.close()
	l.close()

	replay(movetraj)
	print("move done")
	l.open()
	#replay(retreattraj)
	#print("retreat done")
	print(movetraj[-1])
	exit(0)
	
if __name__ == '__main__':
	main()


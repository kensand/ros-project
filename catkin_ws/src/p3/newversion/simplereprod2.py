#!/usr/bin/env python
import rospy
import rosbag
import csv
from intera_core_msgs.msg import (EndpointState, IOComponentCommand)
from geometry_msgs.msg import PoseStamped
from intera_core_msgs.srv import (SolvePositionIK, SolvePositionIKRequest)
from sensor_msgs.msg import JointState
from visualization_msgs.msg import (Marker, MarkerArray)
from std_msgs.msg import String
import intera_interface
import intera_external_devices
import pickle
import argparse
import subprocess
import signal
import os
from intera_interface import CHECK_VERSION
import dmp
import numpy as np
import math
import json
import matplotlib.pyplot as plt
from util import *
jointangletolerance = 0.05
joint_tolerance = 0.01
#calback function for recording a subscriber

def main(graphs=True):
        rospy.init_node('reproduce')
        #graphs=False
        approachpoints, approachtimes = loadbag('simple2.bag')

	#known good parameters, to save time instead of executing policy search every time. Set to None to execute policy search
 	approachparams = [1.2, 0.8, 0.8, 0.8]       

        print(approachpoints.shape)
        
        approachphase = np.array([dmp.calcphase(x, approachtimes[-1]) for x in approachtimes])  
        allphase = np.concatenate((-1 + -1 * approachphase,), axis=0)
        

        reset()
        objtrans = np.array([0.0, -0.3, 0., 0., 0., 0., 0.])
        approachtraj, approachends, approacharr, approachphase = reproduce(approachpoints, approachtimes, endtransform=objtrans + np.array([0., 0.0, 0., 0., 0., 0., 0.]), parameters=approachparams, K=700)
        showPath(approachends)
        newallphase = np.concatenate((-1. - approachphase,), axis=0)
        actual = np.concatenate((approachpoints,), axis=0)
        newtrajs = np.concatenate((approacharr,), axis=0)
        if graphs:
                labels=['x-position', 'y-position', 'z-position', 'x-orientation', 'y-orientation', 'z-orientation', 'w-orientation']
                plotlocs = [1, 2, 3, 5, 6, 7, 8]
                for i in range(actual.shape[1]):
                        
                        plt.subplot(2,4, plotlocs[i])
                        a1 = plt.plot(allphase, actual[:,i], 'r.-', label='Demonstration')
                        a2 = plt.plot(newallphase, newtrajs[:,i], 'b.-', label='Reproduction')
                        if plotlocs[i] == 3:
                                plt.legend(bbox_to_anchor=(1.5, 0.5), loc='center')
                                
                        plt.xlabel("phase")
                        plt.ylabel(labels[i])
                plt.show()

        raw_input("Press enter to execute movement\n")
        l = intera_interface.Gripper('right_gripper')
        l.open()
        replay(approachtraj)
        print("approach done")
        exit(0)

if __name__ == '__main__':
	main()


#!/usr/bin/env python
import rospy
import rosbag
import csv
from intera_core_msgs.msg import EndpointState
from intera_core_msgs.srv import (SolvePositionIK, SolvePositionIKRequest)
from sensor_msgs.msg import JointState
from visualization_msgs.msg import (Marker, MarkerArray)
from std_msgs.msg import String
import intera_interface
import intera_external_devices
import pickle
import argparse
import subprocess
import signal
import os
from intera_interface import CHECK_VERSION
#from rviz_visual_tools import RvizVisualTools

jointangletolerance = 0.01

#calback function for recording a subscriber
points = []
names = []
def callback(data):
	points.append(data)


#main function
def main():

	rospy.init_node('node_name')
	
	#parse args
	ap = argparse.ArgumentParser(prog='p2.py')
	ap.add_argument('-w', '--write', default=None, type=str)
	ap.add_argument('-r', '--read', default=None, type=str)
	ap.add_argument('-p', '--replay', action='store_true')
	ap.add_argument('-v', '--verbose', action='store_true', default=False)
	ap.add_argument('-s', '--secs', default=0, type=int)
	ap.add_argument('-n', '--nsecs', default=100000000, type=int)
	ap.add_argument('-m', '--method', choices=['simple', 'better'], default='better')
	ap.add_argument('-t', '--joint_tolerance', default = 0.05, type=float)
	args = ap.parse_args()

	if args.verbose:
		print("Being Verbose")

	#load from file if needed
	if args.read:
		bag = rosbag.Bag(args.read, 'r')
		for topic, msg, t in bag.read_messages(topics=['/robot/limb/right/endpoint_state']):
			points.append(msg)
		bag.close()
		'''
		with open(args.read, 'r+') as f:
			r = csv.reader(f)
			for row in r:
				es = pickle.loads(row[0])
				points.append(es)
		'''

	#otherwise record trajectory
	else:
		listener()

	#save recording if requested
	bag = None
	if args.write:
		bag = rosbag.Bag(args.write, 'w')
		for point in points:
			bag.write('/robot/limb/right/endpoint_state', point);
		bag.close()

		'''
		with open(args.write, 'w+') as f:
			w = csv.writer(f)
			w.writerows([[pickle.dumps(x)] for x in points])
		'''
	#filter points to follow specified keyframe intervals
	filtered = []
	last = points[0]
	delay = float(args.secs) + float(args.nsecs) / 1000000000.

	for p in points:
		save = False

		time = float(p.header.stamp.secs) + float(p.header.stamp.nsecs) / 1000000000.
		ltime = float(last.header.stamp.secs) + float(last.header.stamp.nsecs) / 1000000000.
		'''
		if p.header.stamp.secs > last.header.stamp.secs + args.secs + 1:
			save = True
		elif p.header.stamp.secs == last.header.stamp.secs + args.secs and p.header.stamp.nsecs > last.header.stamp.nsecs + args.nsecs:
			save = True
		'''
		if time > ltime + delay:
			save = True
		if save:
			np = p.pose
			n = [np.position.x, np.position.y, np.position.z, np.orientation.x, np.orientation.y, np.orientation.z, np.orientation.w]
			np = last.pose
			l = [np.position.x, np.position.y, np.position.z, np.orientation.x, np.orientation.y, np.orientation.z, np.orientation.w]
			if len([x for x,y in zip(n,l) if abs(x-y) > args.joint_tolerance]) > 0:
				filtered.append(p)
				last = p

	showPath(filtered)
	if args.verbose:
		count = 1
		for j in filtered:
			print(str(count) + ": " + str(j))
			count += 1

	#replay if it was requested
	if args.replay:
		joints = None
		if args.method == 'simple':
			print("Using simple IK")
			joints = ik(filtered, args.verbose)
		else:
			print("Using better IK")
			joints = bik(filtered, args.verbose)
		#print(joints)
		raw_input("Press enter to replay")
		replay(joints, args.verbose)

def showPath(p):
	#RvizVisualTools.deleteAllMarkers()
	mpub = rospy.Publisher('visualization_marker', Marker, queue_size=len(p), latch=True)
	pub = rospy.Publisher('visualization_marker_array', MarkerArray, queue_size=len(p), latch=True)
	rate = rospy.Rate(0.01)
	marr = MarkerArray()
	count = 0
	for point in p:
		m = Marker()
		m.id = count
		count += 1
		m.pose = point.pose
		m.color.a = 1.
		m.color.b = (float(len(p)) - float(count)) / float(len(p))
		m.color.r = float(count) / float(len(p))
		#print(m.color.b, m.color.r, count)
		m.color.g = 0.
		m.scale.x =0.1
		m.scale.y = m.scale.z = 0.01
		m.header.frame_id = 'base'
		m.type = m.ARROW
		#print(m)
		marr.markers.append(m)
		mpub.publish(m)
	print("Publishing " + str(len(marr.markers)) + " markers, 'c' to continue.")
	#while intera_external_devices.getch() != 'c':
	
	pub.publish(marr)
	rospy.sleep(0.01)
	#rate.sleep()

def replay(joints, verbose):
	l = intera_interface.Limb('right')
	count = 1
	
	#check if the last location command finished
	def limbdone(limb, pos):
		for n, a in limb.joint_angles().items():
			#print(str(n) + " : " + str(a))
			if abs(a - pos[n]) > jointangletolerance:
				return False
		return True
	
	#move to neutral, then replay each location command
	l.move_to_neutral(timeout=1.)
	for point in joints:
		l.move_to_joint_positions(dict(zip(point.name,point.position)))
		if verbose:
			print(str(count) + ": Moved to " + str(point.position))
		now = rospy.get_time()
		while not limbdone(l, dict(zip(point.name,point.position))) and rospy.get_time() - now < 1.:
			pass
		count += 1
		

#do single Inverse Kinematics (allows for seed pose)
def singleik(endpoint, verbose, seed=None):
	ns = "ExternalTools/right/PositionKinematicsNode/IKService"
	svc = rospy.ServiceProxy(ns, SolvePositionIK)
	req = SolvePositionIKRequest()
	req.pose_stamp.append(endpoint)
	req.tip_names.append('right_hand')
	if seed:
		req.seed_angles.append(seed)
	try:
		rospy.wait_for_service(ns, 30)
		resp = svc(req)
		if verbose:
			print(str(endpoint) + "\nResult: " + str(resp.result_type))
		if resp.result_type[0] > 0:
			return resp
	except Exception, e:
			rospy.logerr(e)
	if verbose:
		print(str(endpoint) + "\nResult: IK failed")
	return False

#better Inverse Kinematic function using the singleik and seeds
def bik(endpoints, verbose):
	ret = []
	i = singleik(endpoints[0], verbose)
	if i:
		ret += i.joints
	for p in endpoints[1:]:

		seed = None
		if len(ret) >0:
			seed = JointState()
			seed.name = ret[len(ret) - 1].name
			seed.position = ret[len(ret) - 1].position
		i = singleik(p, verbose, seed)
		if i:
			ret += i.joints
	return ret

#simple Inverse Kinematics function, solves them all in one service request, does not use seeds
def ik(endpoints, verbose):
	ns = "ExternalTools/right/PositionKinematicsNode/IKService"
	svc = rospy.ServiceProxy(ns, SolvePositionIK)
	req = SolvePositionIKRequest()
	req.pose_stamp += endpoints
	req.tip_names += ['right_hand'] * len(endpoints)
	try:
		rospy.wait_for_service(ns, 5.0)
		resp = svc(req)
		return resp.joints
	except Exception, e:
		rospy.logerr(e)
	return False

#listener function to record the endpoint location
def listener():
	#print("starting sub")
	s = rospy.Subscriber("/robot/limb/right/endpoint_state", EndpointState, callback)
	#print("sub created")
	#rospy.sleep(time)
	map_keyboard('right')
	s.unregister()


#This function was taken DIRECTLY from the intera example for keyboard control.
#I take absolutely no credit for it. It is used simply to move the arm while recording.
#the source can be found at 
# https://github.com/RethinkRobotics/intera_sdk/blob/master/intera_examples/scripts/joint_position_keyboard.py
# The outputs were slightly modified to suit the program
def map_keyboard(side):
	limb = intera_interface.Limb(side)

	try:
		gripper = intera_interface.Gripper(side + '_gripper')
	except:
		has_gripper = False
		rospy.loginfo("The electric gripper is not detected on the robot.")
	else:
		has_gripper = True

	joints = limb.joint_names()

	def set_j(limb, joint_name, delta):
		current_position = limb.joint_angle(joint_name)
		joint_command = {joint_name: current_position + delta}
		limb.set_joint_positions(joint_command)

	def set_g(action):
		if has_gripper:
			if action == "close":
				gripper.close()
			elif action == "open":
				gripper.open()
			elif action == "calibrate":
				gripper.calibrate()

	bindings = {
		'1': (set_j, [limb, joints[0], 0.1], joints[0]+" increase"),
		'q': (set_j, [limb, joints[0], -0.1], joints[0]+" decrease"),
		'2': (set_j, [limb, joints[1], 0.1], joints[1]+" increase"),
		'w': (set_j, [limb, joints[1], -0.1], joints[1]+" decrease"),
		'3': (set_j, [limb, joints[2], 0.1], joints[2]+" increase"),
		'e': (set_j, [limb, joints[2], -0.1], joints[2]+" decrease"),
		'4': (set_j, [limb, joints[3], 0.1], joints[3]+" increase"),
		'r': (set_j, [limb, joints[3], -0.1], joints[3]+" decrease"),
		'5': (set_j, [limb, joints[4], 0.1], joints[4]+" increase"),
		't': (set_j, [limb, joints[4], -0.1], joints[4]+" decrease"),
		'6': (set_j, [limb, joints[5], 0.1], joints[5]+" increase"),
		'y': (set_j, [limb, joints[5], -0.1], joints[5]+" decrease"),
		'7': (set_j, [limb, joints[6], 0.1], joints[6]+" increase"),
		'u': (set_j, [limb, joints[6], -0.1], joints[6]+" decrease")
	 }
	if has_gripper:
		bindings.update({
		'8': (set_g, "close", side+" gripper close"),
		'i': (set_g, "open", side+" gripper open"),
		'9': (set_g, "calibrate", side+" gripper calibrate")
		})
	done = False
	raw_input("Press enter to start recording. ? for help, escape or x to exit.")
	while not done and not rospy.is_shutdown():
		c = intera_external_devices.getch()
		if c:
			#catch Esc or ctrl-c
			if c in ['\x1b', '\x03', 'x']:
				done = True
				return
				#rospy.signal_shutdown("Example finished.")
			elif c in bindings:
				cmd = bindings[c]
				if c == '8' or c == 'i' or c == '9':
					cmd[0](cmd[1])
					print("command: %s" % (cmd[2],))
				else:
					#expand binding to something like "set_j(right, 'j0', 0.1)"
					cmd[0](*cmd[1])
					print("command: %s" % (cmd[2],))
			else:
				print("key bindings: ")
				print("  Esc: Quit")
				print("  ?: Help")
				for key, val in sorted(bindings.items(),
									   key=lambda x: x[1][2]):
					print("  %s: %s" % (key, val[2]))


if __name__ == '__main__':
	main()


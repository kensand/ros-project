#!/usr/bin/env python
import rospy
import rosbag
import csv
from intera_core_msgs.msg import (EndpointState, IOComponentCommand)
from geometry_msgs.msg import PoseStamped
from intera_core_msgs.srv import (SolvePositionIK, SolvePositionIKRequest)
from sensor_msgs.msg import JointState
from visualization_msgs.msg import (Marker, MarkerArray)
from std_msgs.msg import String
import intera_interface
import intera_external_devices
import pickle
import argparse
import subprocess
import signal
import os
from intera_interface import CHECK_VERSION
import dmp
import numpy as np
import math
import json
import matplotlib.pyplot as plt
#from rviz_visual_tools import RvizVisualTools

jointangletolerance = 0.05
joint_tolerance = 0.01
#calback function for recording a subscriber

def main(graphs=True):
        rospy.init_node('reproduce')
        #graphs=False
        approachpoints, approachtimes = loadbag('simple2.bag')
 	approachparams = [1.2, 0.8, 0.8, 0.8]       

        print(approachpoints.shape)
        
        approachphase = np.array([dmp.calcphase(x, approachtimes[-1]) for x in approachtimes])  
        allphase = np.concatenate((-1 + -1 * approachphase,), axis=0)
        

        reset()
        objtrans = np.array([0.0, 0., 0., 0., 0., 0., 0.])
        approachtraj, approachends, approacharr, approachphase = reproduce(approachpoints, approachtimes, endtransform=objtrans + np.array([0., 0.0, 0., 0., 0., 0., 0.]), parameters=approachparams, K=1500)
        showPath(approachends)
        newallphase = np.concatenate((-1. - approachphase,), axis=0)
        actual = np.concatenate((approachpoints,), axis=0)
        newtrajs = np.concatenate((approacharr,), axis=0)
        if graphs:
                labels=['x-position', 'y-position', 'z-position', 'x-orientation', 'y-orientation', 'z-orientation', 'w-orientation']
                plotlocs = [1, 2, 3, 5, 6, 7, 8]
                for i in range(actual.shape[1]):
                        
                        plt.subplot(2,4, plotlocs[i])
                        a1 = plt.plot(allphase, actual[:,i], 'o', label='Demonstration')
                        a2 = plt.plot(newallphase, newtrajs[:,i], 'x', label='Reproduction')
                        if plotlocs[i] == 3:
                                plt.legend(bbox_to_anchor=(1.5, 0.5), loc='best')
                                
                        plt.xlabel("phase")
                        plt.ylabel(labels[i])
                plt.show()

        raw_input("Press enter to execute movement\n")
        l = intera_interface.Gripper('right_gripper')
        l.open()
        replay(approachtraj)
        print("approach done")
        exit(0)
	

def costFunction(orig, origphase, reprod, reprodphase):
	tot = 0.
	for i, t in enumerate(reprodphase):
		opos = dmp.lininter(np.array(origphase), np.array(orig), t)
		dist = np.sqrt(sum((reprod[i] - opos) * (reprod[i] - opos)))
		tot += dist
	return tot / float(len(reprod))
	

#def cubicinterp(x, y, xns):
#	funcs = []
#	for i in range(y.shape[1]):
#		funcs.append(UnivatiateSpline(x, y[:,i]))
#	rets = []
#	for i in range(y.shape[0]):
#		rets.append([f(xns[i]) for f in funcs])
#	return np.array(rets)

def loadbag(bagname):
	points = []
	names = []
	secs = 0.
	nsecs = 10000000
	bag = rosbag.Bag(bagname, 'r')
	for topic, msg, t in bag.read_messages(topics=['/robot/limb/right/endpoint_state']):
		points.append(msg)

	bag.close()
	#save recording if requested
	bag = None

	#filter points to follow specified keyframe intervals
	filtered = []
	filtarr = []
	times = []
	fstime = None
	fntime = None
	last = points[0]
	delay = float(secs) + float(nsecs) / 1000000000.

	for p in points:
		save = False

		time = float(p.header.stamp.secs) + float(p.header.stamp.nsecs) / 1000000000.
		ltime = float(last.header.stamp.secs) + float(last.header.stamp.nsecs) / 1000000000.
		if time > ltime + delay:
			save = True
		if save:
			tmp = p.pose
			n = [tmp.position.x, tmp.position.y, tmp.position.z, tmp.orientation.x, tmp.orientation.y, tmp.orientation.z, tmp.orientation.w]
			tmp = last.pose
			l = [tmp.position.x, tmp.position.y, tmp.position.z, tmp.orientation.x, tmp.orientation.y, tmp.orientation.z, tmp.orientation.w]
			if len([x for x,y in zip(n,l) if abs(x-y) > joint_tolerance]) > 0:

				stime = p.header.stamp.secs
				ntime = p.header.stamp.nsecs
				if not fstime:
					fstime = stime
				if not fntime:
					fntime = ntime
				stime = stime - fstime
				time = (stime * 1000000000. + ntime - fntime) / 1000000000.
				filtered.append(p)
				filtarr.append(n)
				times.append(time)
				last = p


	filtarr = np.array(filtarr)
	times = np.array(times)
	times -= times[0]
	return filtarr, times


def reproduce(endpoints, times, starttransform=np.array([0.] * 7), endtransform=np.array([0.] * 7), startseed=None, parameters=None, K=700, T=None, dt=1.):
	print(endpoints.shape)
	filtarr = endpoints
	startpos = np.array(filtarr[0,:])
	startpos += starttransform
	if not T:
		T = float(len(filtarr))
	endpos = filtarr[-1,:] 
	endpos += endtransform
	dmpparam, phase = dmp.learn(filtarr, times, K)
	#print("dmpparam = ", dmpparam.shape)
	#print("phase=", phase.shape)
	bestcost = 99999999.
	besttraj, bestphase = None, None
	bestsettings = []
	N=5
	r = [0.8, 0.9, 1.0, 1.1, 1.2]
	if not parameters:
		for a in r:
			print(a)
			for b in r:
				for c in r:
					for d in r:
						#print(a, b, c, d)
						newtraj, newphase = dmp.reproduce(dmpparam, phase, startpos, endpos, K, dt, T, a, b, c, d)
						cost = costFunction(newtraj, newphase, filtarr, phase)
						if bestcost > cost:
							bestcost = cost
							besttraj, bestphase = newtraj, newphase
							bestsettings = (a,b,c,d)

		newtraj, newphase = besttraj, bestphase
	else:
		newtraj, newphase = dmp.reproduce(dmpparam, phase, startpos, endpos, K, dt, T, parameters[0], parameters[1], parameters[2], parameters[3])
	newendstates = []
	#print('accel:',newaccels)
	#print('velocity:', newvels)
	#print('demo:', filtarr)
	print('demo startpos:', startpos, 'new startpos:', newtraj[0])
	#print('trajectory:', newtraj)
	print('demo endpos:', endpos, 'new endpos:', newtraj[-1])
	print('best settings: ', bestsettings)
	for point in newtraj:
		s = makePose(point)		
		newendstates.append(s)

	joints = None
	grips = None
	joints = bik(newendstates, startseed)
	#print(bagname + ": " + str(len(newendstates)) + " endstates, " + str(len(joints)) + " jointstates")
	return joints, newendstates, newtraj, np.array(newphase)

def makePose(point):
	s = PoseStamped()
	s.pose.position.x = point[0]
	s.pose.position.y = point[1]
	s.pose.position.z = point[2]
	quat = point[3:7]
	tot = math.sqrt(sum([x * x for x in quat]))
	if tot != 0.:
		quat /= tot
	elif quat[0] == 0. and quat[1] == 0. and quat[2] == 0. and quat[3] == 0.:
		quat = [0., 0., 0., 1.]
	s.pose.orientation.x = quat[0]
	s.pose.orientation.y = quat[1]
	s.pose.orientation.z = quat[2]
	s.pose.orientation.w = quat[3]
	return s

def reset():
	l = intera_interface.Limb('right')
	l.move_to_joint_positions(dict(zip(l.joint_names(), [1.57,  -.78, 0., 0., 0., 0., 0.])), timeout=2.)

def showPath(p):
	#RvizVisualTools.deleteAllMarkers()
	mpub = rospy.Publisher('visualization_marker', Marker, queue_size=len(p), latch=True)
	pub = rospy.Publisher('visualization_marker_array', MarkerArray, queue_size=len(p), latch=True)
	rate = rospy.Rate(0.01)
	marr = MarkerArray()
	count = 0
	for point in p:
		m = Marker()
		m.id = count
		count += 1
		m.pose = point.pose
		m.color.a = 1.
		m.color.b = (float(len(p)) - float(count)) / float(len(p))
		m.color.r = float(count) / float(len(p))
		#print(m.color.b, m.color.r, count)
		m.color.g = 0.
		m.scale.x =0.1
		m.scale.y = m.scale.z = 0.01
		m.header.frame_id = 'base'
		m.type = m.ARROW
		#print(m)
		marr.markers.append(m)
		mpub.publish(m)
	print("Publishing " + str(len(marr.markers)) + " markers, 'c' to continue.")
	#while intera_external_devices.getch() != 'c':
	
	pub.publish(marr)
	rospy.sleep(0.01)
	#rate.sleep()

def replay(joints, verbose=False, dt=0.2):
	l = intera_interface.Limb('right')
	count = 1
	def limbdone(limb, pos):
		for n, a in limb.joint_angles().items():
			#print(str(n) + " : " + str(a))
			#print(a, pos[n], abs(a - pos[n]) > jointangletolerance)
			if abs(a - pos[n]) > jointangletolerance:
				return False
		return True
	
	#move to neutral, then replay each location command
	#l.move_to_neutral(timeout=1.)
	for i, point in enumerate(joints):
		l.set_joint_positions(dict(zip(point.name,point.position)))#, timeout=1.)
		if verbose:
			print(str(count) + ": Moved to " + str(point.position))
		now = rospy.get_time()
		while (not limbdone(l, dict(zip(point.name,point.position)))) and rospy.get_time() - now < dt:
			#rospy.sleep(dt)
			pass
		count += 1
	

#do single Inverse Kinematics (allows for seed pose)
def singleik(endpoint, verbose, seed=None):
	ns = "ExternalTools/right/PositionKinematicsNode/IKService"
	svc = rospy.ServiceProxy(ns, SolvePositionIK)
	req = SolvePositionIKRequest()
	req.pose_stamp.append(endpoint)
	req.tip_names.append('right_hand')
	if seed:
		req.seed_mode = req.SEED_USER
		req.seed_angles.append(seed)
		goal = JointState()
		goal.name = ['right_j0', 'right_j1', 'right_j2']
		goal.position = [seed.position[0], seed.position[1], seed.position[3]]
		#req.nullspace_gain.append(.7)
		#req.nullspace_goal.append(seed)
		#req.use_nullspace_goal.append(True)
	try:
		rospy.wait_for_service(ns, 30)
		resp = svc(req)
		if verbose:
			print(str(endpoint) + "\nResult: " + str(resp.result_type))
		if resp.result_type[0] > 0:
			return resp
	except Exception, e:
			rospy.logerr(e)
	if verbose:
		print(str(endpoint) + "\nResult: IK failed")
	return False

#better Inverse Kinematic function using the singleik and seeds
def bik(endpoints, startseed=None, verbose=False):
	ret = []
	retgrips = []
	i = singleik(endpoints[0], verbose, startseed)
	if i:
		ret += i.joints
	for i,p in enumerate(endpoints[1:]):

		seed = None
		if len(ret) >0:
			seed = JointState()
			seed.name = ret[len(ret) - 1].name
			seed.position = ret[len(ret) - 1].position
		r = singleik(p, verbose, seed)
		if r:
			ret += r.joints
	return ret

#simple Inverse Kinematics function, solves them all in one service request, does not use seeds
def ik(endpoints, verbose):
	ns = "ExternalTools/right/PositionKinematicsNode/IKService"
	svc = rospy.ServiceProxy(ns, SolvePositionIK)
	req = SolvePositionIKRequest()
	req.pose_stamp += endpoints
	req.tip_names += ['right_hand'] * len(endpoints)
	#	req.seed_mode = req.SEED_CURRENT
	try:
		rospy.wait_for_service(ns, 5.0)
		resp = svc(req)
		return resp.joints
	except Exception, e:
		rospy.logerr(e)
	return False

#listener function to record the endpoint location
def listener():
	#print("starting sub")
	s = rospy.Subscriber("/robot/limb/right/endpoint_state", EndpointState, callback)
	s2 = rospy.Subscriber("/io/end_effector/right_gripper/command", IOComponentCommand, gripcallback)
	#print("sub created")
	#rospy.sleep(time)
	map_keyboard('right')
	s.unregister()
	s2.unregister()


#This function was taken DIRECTLY from the intera example for keyboard control.
#I take absolutely no credit for it. It is used simply to move the arm while recording.
#the source can be found at 
# https://github.com/RethinkRobotics/intera_sdk/blob/master/intera_examples/scripts/joint_position_keyboard.py
# The outputs were slightly modified to suit the program
def map_keyboard(side):
	limb = intera_interface.Limb(side)

	try:
		gripper = intera_interface.Gripper(side + '_gripper')
	except:
		has_gripper = False
		rospy.loginfo("The electric gripper is not detected on the robot.")
	else:
		has_gripper = True

	joints = limb.joint_names()

	def set_j(limb, joint_name, delta):
		current_position = limb.joint_angle(joint_name)
		joint_command = {joint_name: current_position + delta}
		limb.set_joint_positions(joint_command)

	def set_g(action):
		if has_gripper:
			if action == "close":
				gripper.close()
			elif action == "open":
				gripper.open()
			elif action == "calibrate":
				gripper.calibrate()

	bindings = {
		'1': (set_j, [limb, joints[0], 0.03], joints[0]+" increase"),
		'q': (set_j, [limb, joints[0], -0.03], joints[0]+" decrease"),
		'2': (set_j, [limb, joints[1], 0.03], joints[1]+" increase"),
		'w': (set_j, [limb, joints[1], -0.03], joints[1]+" decrease"),
		'3': (set_j, [limb, joints[2], 0.03], joints[2]+" increase"),
		'e': (set_j, [limb, joints[2], -0.03], joints[2]+" decrease"),
		'4': (set_j, [limb, joints[3], 0.03], joints[3]+" increase"),
		'r': (set_j, [limb, joints[3], -0.03], joints[3]+" decrease"),
		'5': (set_j, [limb, joints[4], 0.03], joints[4]+" increase"),
		't': (set_j, [limb, joints[4], -0.03], joints[4]+" decrease"),
		'6': (set_j, [limb, joints[5], 0.03], joints[5]+" increase"),
		'y': (set_j, [limb, joints[5], -0.03], joints[5]+" decrease"),
		'7': (set_j, [limb, joints[6], 0.03], joints[6]+" increase"),
		'u': (set_j, [limb, joints[6], -0.03], joints[6]+" decrease")
	 }
	if has_gripper:
		bindings.update({
		'8': (set_g, "close", side+" gripper close"),
		'i': (set_g, "open", side+" gripper open"),
		'9': (set_g, "calibrate", side+" gripper calibrate")
		})
	done = False
	raw_input("Press enter to start recording. ? for help, escape or x to exit.")
	while not done and not rospy.is_shutdown():
		c = intera_external_devices.getch()
		if c:
			#catch Esc or ctrl-c
			if c in ['\x1b', '\x03', 'x']:
				done = True
				return
				#rospy.signal_shutdown("Example finished.")
			elif c in bindings:
				cmd = bindings[c]
				if c == '8' or c == 'i' or c == '9':
					cmd[0](cmd[1])
					print("command: %s" % (cmd[2],))
				else:
					#expand binding to something like "set_j(right, 'j0', 0.1)"
					cmd[0](*cmd[1])
					print("command: %s" % (cmd[2],))
			else:
				print("key bindings: ")
				print("  Esc: Quit")
				print("  ?: Help")
				for key, val in sorted(bindings.items(),
									   key=lambda x: x[1][2]):
					print("  %s: %s" % (key, val[2]))


if __name__ == '__main__':
	main()


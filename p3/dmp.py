import numpy as np
import math
import matplotlib.pyplot as plt


def phase(time, tottime, alpha=(-1. * math.log(0.01))):
	t = math.exp(-1. * alpha * time / tottime)
	return t

def lininter(x,y,xn):
	print(x.shape, y.shape, xn)
	if xn in x:
		return y[x.tolist().index(xn)]
	for v in range (len(x) - 1):
		if xn < x[v] and x[v+1] < xn:
			return y[v] + (xn - x[v]) * (y[v+1]-y[v]) / (x[v+1] - x[v])


def learn(traj, times, k):
	
	times -= times[0] # time starts at 0
	tau = times[-1]	 # tau is the total interval
	dt = np.swapaxes(np.repeat([np.diff(times, n = 1,axis = 0)], traj.shape[1], 0), 0, 1 ) #calculate the time steps

	v = np.diff(traj, n = 1, axis = 0) / dt	#dv/dt
	a = np.diff(v, n=1, axis = 0) / dt[:-1,:] #ddv/dt
	a = np.concatenate((a,  -1. * v[-1:] / dt[-1:], np.zeros((1,traj.shape[1]))), axis=0)#make a and v equal length
	v = np.concatenate((v, np.zeros((1,traj.shape[1]))), axis=0)

	#print("a=", a)
	#print("v=", v)

	d = 2 * math.sqrt(k)
	phase = np.array([calPhase(t,tau) for t in times]) #calculate the phases
	plt.plot(times, phase)
	plt.ylabel("times")
	plt.xlabel("phase")
	plt.show()
	for i in range(a.shape[1]):
		plt.plot(times, a[:,i])
		plt.ylabel("accel")
		plt.xlabel("time")
		plt.show()
		plt.plot(times, v[:,i])
		plt.ylabel("velocity")
		plt.xlabel("time")
		plt.show()

	goal = np.repeat([traj[-1]] , traj.shape[0], axis = 0) #set the goal to be the last point in traj
	x0 = np.repeat([traj[0]], traj.shape[0], axis=0) #set x0 to be the first point in traj
	phasearr = np.swapaxes(np.repeat([phase], traj.shape[1], axis=0), 0, 1) #repeat the phases to make vectors

	#the main learning equation
	f1 = a * tau
	f2 = d * v
	f3 = (goal - traj)
	f4 = (goal - x0) * phasearr
	ftar = ((f1 + f2) / k) - f3 + f4
	for i in range(ftar.shape[1]):
		plt.plot(ftar[:,i], times)
		plt.xlabel("ftar" + str(i))
		plt.ylabel("phase")
		plt.show()

	return ftar, phase
 
def reproduce(ftar, phase, start, goal, k, dt, tau):
	#generate new times, and the phases for them
	tsteps = math.floor(tau/dt)
	newphase = [calPhase(t*dt, tau) for t in range (int(tsteps))]
	

	#linearly interpolate the new ftar
	newftar = [lininter(phase, ftar, s) for s in newphase]

	#initialize the starting x,v,a
	x = start
	v = np.zeros(ftar.shape[1])
	a = np.zeros(ftar.shape[1])

	#loop through each timestep and update a,v,x
	result = []
	d = 2 * math.sqrt(k)
	for i, f in enumerate(newftar):
		g1 = k * (goal-x)
		g2 = d * v
		g3 = k * (goal - start) * newphase[i]
		g4 = k * f
		a = (g1-g2-g3+g4) / tau
		xdot = v / tau
		v = v + a * dt
		x = x + xdot * dt
		result.append(x)
	return np.array(result)

def calPhase(t, tau):
	alpha = -1.*math.log(0.01)
	return math.exp((-alpha/tau)*t)	

def main3():   
	N = 10000
	steps = [0.1 * x for x in range(N)]
	path = [[1.0, 1.0], [2., 2.], [3., 4.], [6., 8.]]
	best = 100.
	bestk = 0.
	besttraj = None
	for k in steps:
		K = k
		D = 2. * math.sqrt(K)
		traj = np.array(path)
		print("Original Trajectory: \n",traj)
		dmp, taus = learn(traj, np.array(range(4)), K)
		print("DMPparameters: \n",dmp)
		restraj = reproduce(dmp, taus, np.array([1., 1.]), np.array([6., 8.]), K, 1., 8.)
		print("Result Trajectory: \n", restraj)
		if sum(np.sqrt((restraj[-1] - traj[-1]) * (restraj[-1] - traj[-1]))) < best:
			best = sum(np.sqrt((restraj[-1] - traj[-1]) * (restraj[-1] - traj[-1])))
			bestk = k
			besttraj = restraj
	
	print(besttraj, bestk, best)	

def main():   
	N = 100
	steps = [float(x) / 10 for x in range(N)]
	path = [[1.0, 1.0], [2., 2.], [3., 4.], [6., 8.]]
	K = 100.
	D = 2. * math.sqrt(K)
	traj = np.array(path)
	print("Original Trajectory: \n",traj)
	dmp, taus = learn(traj, np.array(range(4)), K)
	print("DMPparameters: \n",dmp)
	restraj = reproduce(dmp, taus, np.array([1., 1.]), np.array([6., 8.]), K, 1., 8.)
	print("Result Trajectory: \n", restraj)
	restraj = reproduce(dmp, taus, np.array([1., 1.]), np.array([8., 7.]), K, 1., 8.)
	print("Result Trajectory: \n", restraj)

import random
def main2():   
	N = 100.
	scale = 0.01
	steps = [1.+scale * float(x) /float(N) for x in range(int(N))]
	path = [[i, i*i-5*i + 8] for i in steps]
	K = 100.
	traj = np.array(path)
	print("Original Trajectory: \n",traj)
	dmp, taus = learn(traj, np.array(range(int(N))), K)
	print("DMPparameters: \n",dmp)
	restraj = reproduce( dmp, taus, np.array([1., 1.]), np.array([scale, scale * scale]), K, 1., N)
	print("Result Trajectory: \n", restraj)
	restraj = reproduce(dmp, taus, np.array([1., 1.]), np.array([0.9 * scale, 1.05 * scale * scale]), K, 1., 100 *N)
	print("Result Trajectory: \n", restraj)









if __name__ == "__main__":
	main()



